/*
    Symptome Severe:: [
        s1,
        s2,
        s3, 
        s4,
        s5
    ]
*/
let w1 = [];
let w2 = [];
const r = 0.01;
function net (symp, w1, w2) {
    let act = [];
    for (let i = 0; i < symp.length; i++) {
        act.push(w1[i]*symp[i]+w2[i]);
    }
    return act;
}
function init(symp) {
    for (let i = 0; i < symp.length; i++) {
        w1.push(1);
        w2.push(1);
        // Initialize the weights
    }
}
function train(symp, d) {
    let z = net(symp, w1, w2);
    let ans;
    for (let i =0; i < z.length; i++) {
        ans+=z[i];
    }
    console.log(ans);
    for (let i = 0; i < z.length; i++) {
        w1[i]+=r*((d[i]-z[i])*symp[i]);
        w2[i]+=r*((d[i]-z[i])*w2[i]);
    }
    return Math.round(ans);
}
function go1() {
    let symp = [1, 1, 0, 0, 0];
    init(symp);
    console.log(w1, w2);
    train(symp, [1, 1, 1, 1, 1]);
    console.log(w1, w2);
}
function go2() {
    let symp = [1, 1, 0, 0, 0];
    train(symp, [1, 1, 1, 1, 1]);
    console.log(w1, w2);
}
function go3() {
    let symp = [1, 1, 0, 0, 0];
    train(symp, [0, 0, 0, 0, 0]);
    console.log(w1, w2);
}
// go1();
// for (let i = 0; i < 500; i++) {
//     go2();
//     go3();
// }
let dataset = [];
let anset = [];
for (var i = 0; i < 500; i++) {
    dataset.push([Math.round(Math.random()), Math.round(Math.random()), Math.round(Math.random()), Math.round(Math.random()), Math.round(Math.random()), Math.round(Math.random())]);
    let k = Math.round(Math.random());
    for (let j = 0; j < 6; j++) anset.push([k, k, k, k, k, k]);
}
init(dataset[0]);
for (let i = 0; i < dataset.length; i++) {
    let y = train(dataset[i], anset[i]);
    console.log("FIRST");
    console.log(w1);
    console.log("SECOND");
    console.log(w2);
    console.log("ANSWER");
    console.log(dataset[i][0], y);
}