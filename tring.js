var deepData = {
    flu: {
        basicData: ["fever", "cough", "sore throat", "runny/stuffy nose", "muscle/body aches", "headches", "fatigue", "vomiting", "diarrhea"],
        previousVictims: [
            {
                gender: "f",
                age: 10,
                symptoms: ["fever"],
                recurring: false
            }
        ]
    },
    stroke: {
        basicData: ["numbness", "confusion", "trouble seeing", "trouble walking", "severe headache"],
        previousVictims: [
            {
                gender: "m",
                age: 50,
                symptoms: ["fever"],
                recurring: true
            },
            {
                gender: "f",
                age: 55,
                symptoms: ["confusion"],
                recurring: false
            },
            {
                gender: "f",
                age: 55,
                symptoms: ["confusion", "fatigue"],
                recurring: false
            }
        ]
    }
}
let us = ["cough", "sleep"];
let ill = Object.getOwnPropertyNames(deepData);
let val = [];
// console.log(ill);
let st = deepData;
for (let i = 0; i < ill.length; i++) {
    let elements = (st[ill[i]].basicData);
    let q = 0
    for (let j = 0; j < elements.length; j++) {
        for (let k = 0; k < us.length; k++) {
            if (elements[j] == us[k]) {
                q++;
            }
        }
    }
    val.push(q);
}
console.log(val);
let per = [[['f', 1], ['m', 0.5]], [[50, 1], [60, 0.5]]];
let user = ['f', 60, ["cough", "sleep"]];

let uday = [];
for (let i = 0; i < per.length; i++) {
    for (let j= 0; j < per[i].length; j++) {
        if (per[i][j][0] == user[i]) {
            uday.push(per[i][j][1]);
        }
    }
}
// console.log(uday);