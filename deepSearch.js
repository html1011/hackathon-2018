var deepData = {
    flu: {
        basicData: ["fever", "cough", "sore throat", "runny/stuffy nose", "muscle/body aches", "headches", "fatigue", "vomiting", "diarrhea"],
        previousVictims: [
            {
                gender: "f",
                age: 10,
                symptoms: ["fever"],
                recurring: true
            }
        ]
    },
    stroke: {
        basicData: ["numbness", "confusion", "trouble seeing", "trouble walking", "severe headache"],
        previousVictims: [
            {
                gender: "m",
                age: 50,
                symptoms: ["fever", "trouble seeing", "trouble walking", "numbness"],
                recurring: true
            },
            {
                gender: "f",
                age: 10,
                symptoms: ["confusion"],
                recurring: false
            },
            {
                gender: "f",
                age: 55,
                symptoms: ["confusion", "fatigue"],
                recurring: false
            }
        ]
    }
}
// What we need to do is look at the database we have, and the user.
function deepSearch(database, user) {
    // Now we need to look at the people in each segment.
    var illnesses = Object.getOwnPropertyNames(database),
    ans = [];
    for (var i = 0; i < illnesses.length; i++) {
        // Now we read them, pushing a list of all their values
        var illnessVal = database[illnesses[i]].previousVictims,
            basicLayout = {};
        //Okay. We now know that we have a list of illness values. Let's try to
        //Figure out some weights; we can do that later, though.
        for (var z = 0; z < illnessVal.length; z++) {
            for (var ii = 0; ii < Object.getOwnPropertyNames(illnessVal[z]).length; ii++) {
                var val = Object.getOwnPropertyNames(illnessVal[z])[ii];
                //for(var zz = 1; zz < ;
                if(!basicLayout[val + illnesses[i]]) {
                    basicLayout[val + illnesses[i]] = [];
                }
                if(typeof illnessVal[z][val] == "object") {
                    illnessVal[z][val] = illnessVal[z][val];
                    basicLayout[val + illnesses[i]] = basicLayout[val + illnesses[i]].concat(illnessVal[z][val]);
                }
                else {
                    basicLayout[val + illnesses[i]].push(illnessVal[z][val]);
                }
            }
        }
            //console.log(basicLayout);
        var termsInBasic = Object.getOwnPropertyNames(basicLayout);
        for(var ii = 0; ii < termsInBasic.length; ii++) {
            //console.log(basicLayout[termsInBasic[ii]]);
            basicLayout[termsInBasic[ii]] = bow(basicLayout[termsInBasic[ii]]);
        }
        //console.log(basicLayout);
        // Okay, we got this! Now, all we need to do is loop through user.
        var userTerms = Object.getOwnPropertyNames(user),
        points = 0;
        for(var ii = 0; ii < userTerms.length; ii++) {
            var percents = basicLayout[userTerms[ii] + illnesses[i]];
            for(var z = 0; z < percents.length; z++) {
                if(typeof user[userTerms[ii]] == "object") {
                    for(var zz = 0; zz < user[userTerms[ii]].length; zz++) {
                        if(user[userTerms[ii]][zz][0] == percents[z][0]) {
                            points += percents[z][1] * user[userTerms[ii]][zz][1];
                        }
                    }
                }
                else if(user[userTerms[ii]] == percents[z][0]) {
                    points += percents[z][1];
                }
            }
        }
        ans.push([illnesses[i], points]);
    }
    console.log(ans);
    function bow(arr) {
        var ans = [],
        num = 1,
        myArr = arr.slice().sort();
        while(myArr.length) {
            if(myArr[0] == myArr[1]) {
                num++;
            }
            else {
                ans.push([myArr[0], num]);
                num = 1;
            }
            myArr.splice(0, 1);
        }
        var bigNum = ans.sort(function(a, b) {
            return b[1] - a[1];
        })[0][1];
        //console.log(bigNum);
        for(var i = 0; i < ans.length; i++) {
            ans[i][1] /= bigNum;
        }
        return ans;
    }
}
var fs = require("fs");
fs.readFile("./user.json", "utf8", function (err, data) {
    if (err) throw err;
    var d = JSON.parse(data.trim());
    deepSearch(deepData, d);
});